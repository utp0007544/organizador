package com.example.tumbaiorganizer

import android.app.Fragment
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.view.Menu
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import com.example.tumbaiorganizer.databinding.ActivityMainBinding
import com.example.tumbaiorganizer.ui.addtareaact.AddTareaActivity
import com.example.tumbaiorganizer.ui.detailstarea.DetailsTareaFragment
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    val lblUsername : TextView by lazy { findViewById(R.id.lblShowUsername) }
    val lblEmail : TextView by lazy { findViewById(R.id.lblShowEmail) }
    val btnCerrarSesion : Button by lazy { findViewById(R.id.btnCerrarSesion) }
    val tokenAPI by lazy { intent.getStringExtra("token") }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarMain.toolbar)

        if (Build.VERSION.SDK_INT > 9) {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }

        binding.appBarMain.fab.setOnClickListener { view ->
            //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            //    .setAction("Action", null).show()
            val intent = Intent(this, AddTareaActivity::class.java)
                .putExtra("token",tokenAPI)
            startActivity(intent)
        }
        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)



    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        lblUsername.text = intent.getStringExtra("username").toString()
        lblEmail.text = intent.getStringExtra("email").toString()
        btnCerrarSesion.setOnClickListener {
            val okHttpClient = OkHttpClient()
            val request = Request.Builder()
                .url("http://20.97.115.3/organizzdorapi/public/api/logout")
                .addHeader("Authorization", "Bearer " + tokenAPI)
                .post("".toRequestBody())
                .build()

            okHttpClient.newCall(request).execute().use { response ->
                if (!response.isSuccessful) {
                    Toast.makeText(this,"Ha ocurrido un problema al cerrar sesión",Toast.LENGTH_LONG).show()
                } else {
                    //lblUsername.text = response.body!!.string()
                    val json = JSONObject(response.body!!.string())
                    Toast.makeText(this,json["message"].toString(),Toast.LENGTH_LONG).show()
                    finish()
                }
            }
        }
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

}